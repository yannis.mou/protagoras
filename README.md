# Protagoras - Machine Learning For Trading

##Before You Start

Learn a few things about [Protagoras](http://en.wikipedia.org/wiki/Protagoras), that amazing sophist and his famous quote "Man is the measure of all things".


## The Cool Stuff Now

**Protagoras has shown some interesting results for daily stock market predictions, however, in Forex exchange and intra-day trading it is not ready yet.

The development of Protagoras started as an experiment to develop a stock market simulator using [QSTK](http://wiki.quantsoftware.org/) and Yahoo data.

From there I wanted to explore how traditional Machine Learning algorithms, e.g., Support Vector Machines, would actually perform and extend it to work with Metatrader to use ML in real trading.

###Most of the code is undocumented and was intented for personal use, however, after discussions with other people I realized it may be useful to them. I have decided to open source it until I find the time to transform it to the project it deserves to be.

###Only if you are a developer you should consider using this software.



##Quick Start

#####Please note that the following guidelines have not been confirmed and are subject to fierce code development! Please let me know if you have any problems by visiting my [webpage](http://www.pierris.gr)

##Working with QSTK - Market Simulation
To use Protagoras with QSTK as a backend you should first [install QSTK](http://wiki.quantsoftware.org/index.php?title=QSToolKit_Installation_Guide) and its prerequisites.

From there you could run Protagoras like that:
```
python main.py --cash 100000 --orders=orders.csv --v values.csv
```

See more info below from the usage function

```
-c cash          The starting amount of cash, e.g., -c 1000000, or --cash=1000000
-o orders.csv          The orders' file input, see orders.csv for format. Also --orders=orders.csv
-v values.csv          The output file name. Does not need to exist.It will override previous simulations if filename is the same! Also --values=values.csv

```

##Working with QSTK - SVM Learning for Market Predictions

To use Protagoras with QSTK as a backend you should first [install QSTK](http://wiki.quantsoftware.org/index.php?title=QSToolKit_Installation_Guide) and its prerequisites.

From there you could run Protagoras like that:

Open the learningDemo.py file and add the symbols you want the SVM to learn. I also provide a complete list below that line for your convenience. I would take a few hours to complete the full list though.

e.g.,    symbols = ['AAPL', 'GOOG']

and then run

```
python learningDemo.py
```

that should fetch the data from yahoo finance, perform PCA, learn an SVM model and simulate it so you can see some results.



#Working with Metatrader

The whole idea is that Metatrader downloads historical data (See the [other related project](https://bitbucket.org/gpierris/protagoras-expert-advisor) ), Protagoras learns a prediction model using SVM (or any other algorithm you want from the mlpy package) and then performs predictions.

The complete process should follow:

###Get data
Run GetHistoricalData.mq5 [(for more info)](https://bitbucket.org/gpierris/protagoras-expert-advisor) and locate the file with the historical data, e.g., 
```C:\ProgramData\MetaQuotes\Terminal\Common\Files\data\HistoricalData_2013.01.01_To_2013.02.15.txt```

###Learn Model
Change the dataFile variable at line 56 to reflect to your path of the historical data, e.g., 
```
dataFile = "C:\\ProgramData\\MetaQuotes\\Terminal\\Common\\Files\\data\\HistoricalData_2013.01.01_To_2013.02.15.txt"
```

and then

```
python MT5_Training.py
```
That should augment the desired training data with their speeds and accelerations according to your needs, e.g., see the derivativeOf function and its use in the code (or maybe commented code), then increases the dimensionality by cross-multiplying the features, decreases the dimensionality by PCA and saves the model using pickle module. 

Saving the exact model is really important as everything is learned in the latent space, however, as requests for prediction will come in the original space; therefore, we have to faithfully transform it in order to get a "valid" prediction.

###Predict and Test and a bit of Magic

Metatrader 5 and Protagoras communicate in what I call "a lagged" model. At every timestep (you define the period, e.g., 1M, 1H etc.) our expert advisor ([again look at the other project](https://bitbucket.org/gpierris/protagoras-expert-advisor)) submits a request by fetching the current state of the market and giving an increasing id, e.g., at file prediction_request_15. Protagoras has been waiting already that file to be created and when this happens, it produces a prediction file which is then read by Metatrader 5 and performs the appropriate action, i.e., buy or sell.

Well, it may not allow millisecond executions, but it is quite fast for the amateur quant.

Run 
```
python MT5_prediction.py

```

but first change the following two lines to correspond to your configuration so that protagoras knows where to find the requests and where to post the result

```
predictionBasePath = 'C:\\ProgramData\\MetaQuotes\\Terminal\\Common\\Files\\data\\'
predictionResultPath = "C:\\Users\\gpierris\\AppData\\Roaming\\MetaQuotes\\Tester\\D0E8209F77C8CF37AD8BF550E51FF075\\Agent-127.0.0.1-3000\\MQL5\\Files\\"

```

#Initial Results

![alt text](https://dl.dropboxusercontent.com/u/5620851/initialResults.png "" Initial Results")

#But Always Remember - Academics trying Quantitave Trading


![alt text](http://gifs.gifbin.com/102012/1350322998_girl_vs_pinata_fail.gif "" README.md URL")
