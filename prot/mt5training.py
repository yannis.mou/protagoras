'''
The MIT License (MIT)

Copyright (c) 2013 Georgios Pierris - www.pierris.gr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''

#import datetime as dt
#import random
#import sys
import numpy as np

import pickle

try:
    import mlpy
    mlpyAvailable = True
except:
    print 'mlpy is not supported in this machine'
    mlpyAvailable = False

from prot import marketdata_accessor as MDA
from prot import datapreparation as dp

#from prot import predictionsfromsvm_model

def runSim():

    #symbols = ['A', 'AA', 'AAPL', 'ABC', 'ABT', 'ACE', 'ACN', 'ADBE', 'ADI', 'ADM', 'ADP', 'ADSK', 'AEE', 'AEP', 'AES', 'AET', 'AFL', 'AGN', 'AIG', 'AIV', 'AIZ', 'AKAM', 'ALL', 'ALTR', 'ALXN', 'AMAT', 'AMD', 'AMGN', 'AMP', 'AMT', 'AMZN', 'AN', 'ANF', 'ANR', 'AON', 'APA', 'APC', 'APD', 'APH', 'APOL', 'ARG', 'ATI', 'AVB', 'AVP', 'AVY', 'AXP', 'AZO', 'BA', 'BAC', 'BAX', 'BBBY', 'BBT', 'BBY', 'BCR', 'BDX', 'BEAM', 'BEN', 'BF.B', 'BHI', 'BIG', 'BIIB', 'BK', 'BLK', 'BLL', 'BMC', 'BMS', 'BMY', 'BRCM', 'BRK.B', 'BSX', 'BTU', 'BWA', 'BXP', 'C', 'CA', 'CAG', 'CAH', 'CAM', 'CAT', 'CB', 'CBE', 'CBG', 'CBS', 'CCE', 'CCI', 'CCL', 'CELG', 'CERN', 'CF', 'CFN', 'CHK', 'CHRW', 'CI', 'CINF', 'CL', 'CLF', 'CLX', 'CMA', 'CMCSA', 'CME', 'CMG', 'CMI', 'CMS', 'CNP', 'CNX', 'COF', 'COG', 'COH', 'COL', 'COP', 'COST', 'COV', 'CPB', 'CRM', 'CSC', 'CSCO', 'CSX', 'CTAS', 'CTL', 'CTSH', 'CTXS', 'CVC', 'CVH', 'CVS', 'CVX', 'D', 'DD', 'DE', 'DELL', 'DF', 'DFS', 'DGX', 'DHI', 'DHR', 'DIS', 'DISCA', 'DLTR', 'DNB', 'DNR', 'DO', 'DOV', 'DOW', 'DPS', 'DRI', 'DTE', 'DTV', 'DUK', 'DV', 'DVA', 'DVN', 'EA', 'EBAY', 'ECL', 'ED', 'EFX', 'EIX', 'EL', 'EMC', 'EMN', 'EMR', 'EOG', 'EQR', 'EQT', 'ESRX', 'ESV', 'ETFC', 'ETN', 'ETR', 'EW', 'EXC', 'EXPD', 'EXPE', 'F', 'FAST', 'FCX', 'FDO', 'FDX', 'FE', 'FFIV', 'FHN', 'FII', 'FIS', 'FISV', 'FITB', 'FLIR', 'FLR', 'FLS', 'FMC', 'FOSL', 'FRX', 'FSLR', 'FTI', 'FTR', 'GAS', 'GCI', 'GD', 'GE', 'GILD', 'GIS', 'GLW', 'GME', 'GNW', 'GOOG', 'GPC', 'GPS', 'GS', 'GT', 'GWW', 'HAL', 'HAR', 'HAS', 'HBAN', 'HCBK', 'HCN', 'HCP', 'HD', 'HES', 'HIG', 'HNZ', 'HOG', 'HON', 'HOT', 'HP', 'HPQ', 'HRB', 'HRL', 'HRS', 'HSP', 'HST', 'HSY', 'HUM', 'IBM', 'ICE', 'IFF', 'IGT', 'INTC', 'INTU', 'IP', 'IPG', 'IR', 'IRM', 'ISRG', 'ITW', 'IVZ', 'JBL', 'JCI', 'JCP', 'JDSU', 'JEC', 'JNJ', 'JNPR', 'JOY', 'JPM', 'JWN', 'K', 'KEY', 'KFT', 'KIM', 'KLAC', 'KMB', 'KMI', 'KMX', 'KO', 'KR', 'KSS', 'L', 'LEG', 'LEN', 'LH', 'LIFE', 'LLL', 'LLTC', 'LLY', 'LM', 'LMT', 'LNC', 'LO', 'LOW', 'LRCX', 'LSI', 'LTD', 'LUK', 'LUV', 'LXK', 'LYB', 'M', 'MA', 'MAR', 'MAS', 'MAT', 'MCD', 'MCHP', 'MCK', 'MCO', 'MDT', 'MET', 'MHP', 'MJN', 'MKC', 'MMC', 'MMM', 'MNST', 'MO', 'MOLX', 'MON', 'MOS', 'MPC', 'MRK', 'MRO', 'MS', 'MSFT', 'MSI', 'MTB', 'MU', 'MUR', 'MWV', 'MYL', 'NBL', 'NBR', 'NDAQ', 'NE', 'NEE', 'NEM', 'NFLX', 'NFX', 'NI', 'NKE', 'NOC', 'NOV', 'NRG', 'NSC', 'NTAP', 'NTRS', 'NU', 'NUE', 'NVDA', 'NWL', 'NWSA', 'NYX', 'OI', 'OKE', 'OMC', 'ORCL', 'ORLY', 'OXY', 'PAYX', 'PBCT', 'PBI', 'PCAR', 'PCG', 'PCL', 'PCLN', 'PCP', 'PCS', 'PDCO', 'PEG', 'PEP', 'PFE', 'PFG', 'PG', 'PGR', 'PH', 'PHM', 'PKI', 'PLD', 'PLL', 'PM', 'PNC', 'PNW', 'POM', 'PPG', 'PPL', 'PRGO', 'PRU', 'PSA', 'PSX', 'PWR', 'PX', 'PXD', 'QCOM', 'QEP', 'R', 'RAI', 'RDC', 'RF', 'RHI', 'RHT', 'RL', 'ROK', 'ROP', 'ROST', 'RRC', 'RRD', 'RSG', 'RTN', 'S', 'SAI', 'SBUX', 'SCG', 'SCHW', 'SE', 'SEE', 'SHLD', 'SHW', 'SIAL', 'SJM', 'SLB', 'SLM', 'SNA', 'SNDK', 'SNI', 'SO', 'SPG', 'SPLS', 'SRCL', 'SRE', 'STI', 'STJ', 'STT', 'STX', 'STZ', 'SUN', 'SWK', 'SWN', 'SWY', 'SYK', 'SYMC', 'SYY', 'T', 'TAP', 'TDC', 'TE', 'TEG', 'TEL', 'TER', 'TGT', 'THC', 'TIE', 'TIF', 'TJX', 'TMK', 'TMO', 'TRIP', 'TROW', 'TRV', 'TSN', 'TSO', 'TSS', 'TWC', 'TXN', 'TXT', 'TYC', 'UNH', 'UNM', 'UNP', 'UPS', 'URBN', 'USB', 'UTX', 'V', 'VAR', 'VFC', 'VIAB', 'VLO', 'VMC', 'VNO', 'VRSN', 'VTR', 'VZ', 'WAG', 'WAT', 'WDC', 'WEC', 'WFC', 'WFM', 'WHR', 'WIN', 'WLP', 'WM', 'WMB', 'WMT', 'WPI', 'WPO', 'WPX', 'WU', 'WY', 'WYN', 'WYNN', 'X', 'XEL', 'XL', 'XLNX', 'XOM', 'XRAY', 'XRX', 'XYL', 'YHOO', 'YUM', 'ZION', 'ZMH']
    symbols = ['EURUSD']
    allRet, allSharpe = [], []

    for symbol in symbols:
        ret, sharpe = main(symbol)
        allRet.append(ret)
        allSharpe.append(sharpe)

    print '\n\nAll Returns: ', allRet
    print '\n\nAll Sharpe: ', allSharpe
    print '\n\nAll Returns (avergage - std): ', np.average(allRet),'-', np.std(allRet)
    print '\n\nAll Sharpe (average - std): ', np.average(allSharpe),'-', np.std(allSharpe)


def main(stockSymbol='EURUSD'):

    performPCA = True
    myPCA = []
    firstDimensions = 0

    svmModelOut = 'svm_model.dat'

    barsBack = 1

# Do exactly the same thing for Prediction.... FIXME: Refactor code here!
    marketData = MDA.MarketDataAccessor()

    symbols = marketData.set_symbols([stockSymbol])

    #Fetch market data all in once!
    print 'Please be patient... Fetching data...\n For symbols: ', symbols
    #print 'From', startDay, ' until ', endDay, '\nfor symbols:\n', symbols
    #DATA_FILE = "C:\\Users\\gpierris\\AppData\\Roaming\\MetaQuotes\\Terminal\\D0E8209F77C8CF37AD8BF550E51FF075\\MQL5\\Files\\data\\HistoricalData_2013.01.01_To_2013.12.31.txt"
    DATA_FILE = "C:\\ProgramData\\MetaQuotes\\Terminal\\Common\\Files\\data\\HistoricalData_2013.01.01_To_2013.02.15.txt"
    marketData.load_data_from_mt5(DATA_FILE, 'EURUSD')
    print '\nFetching data completed. Execution continues...\n\n'

    #raw_input('Hit Enter to continue...')

#We need only a subsection for learning, not all the sp500
    #symbolsForLearning = marketData.set_symbols([stockSymbol])

#Data preparation begins...
    print '\nPreparing data for SVM...'
    dataPrep = dp.DataPreparation(marketData)

    #x, y = dataPrep.prepareData_lastPrices(daysBack=barsBack)
    x, y = dataPrep.prepareData_AllFeatures(daysBack=barsBack, barsAhead = 5)
    if(np.isnan(np.sum(x))):
        return -0.0, -0.0
    print 'Debug 1'
    print x
    print 'Debug 2'
    print y
    print 'Debug 3'
    y = dataPrep.derivativeOf(y)
    print 'Debug 4\n\n', y
    y = dataPrep.defineClasses(y, thresholds = [1.0, 1.0])
    print y
#print x[:3]
#print np.size(x[:,0]), np.size(x[0,:])
#raw_input('press enter to continue...')

    #feature = dataPrep.getFeature_StockOrIndex('EURUSD', daysBack=barsBack)
    #x = dataPrep.appendColumns(x, feature)

#print x[:3]
#print np.size(x[:,0]), np.size(x[0,:])


    #x = dataPrep.derivativeOf(x)
    #x = dataPrep.appendColumns(x, dataPrep.derivativeOf(x))
#print x[:3]
#print np.size(x[:,0]), np.size(x[0,:])


#print '------------------------------'
    x = dataPrep.increaseDimensionality(x)
#x = dataPrep.increaseDimensionality(x)
#print x
#print np.size(x[:,0]), np.size(x[0,:])
#print '---------------------------------'


    try:
        x_row = np.size(x[:,0])
    except:
        x_row = np.size(x[:])

    try:
        x_col = np.size(x[0,:])
    except:
        x_col = 1

    try:
        y_row = np.size(y[:,0])
    except:
        y_row = np.size(y[:])

    try:
        y_col = np.size(y[0,:])
    except:
        y_col = 1
    print 'x size: ', x_row, ' by ', x_col, '\ty size: ', y_row, ' by ', y_col

#print 'Normalizing Features'
#x_norm = (x - np.mean(x))/(np.sqrt(np.sum((x)**2, axis=0)))
#print x_norm

    if(performPCA):
        #Perform PCA
        myPCA = mlpy.PCA()
        myPCA.learn(x)
        #coeff = myPCA.coeff()
        #print coeff
        vec = np.array(myPCA.evals())
        print vec
        firstDimensions = np.where( vec <= 0.00001)[0][0]
        #firstDimensions = len(vec)
        print 'First dimensions: ', firstDimensions
        z = myPCA.transform(x, firstDimensions)
        print '\nData size after PCA: ', z.shape[0], 'by', z.shape[1]

    print '\nData preparation ended.'

# Initializing the SVM
    print '\nInitializing SVM...'
    svm = mlpy.libsvm.LibSvm(svm_type='c_svc', kernel_type='rbf', C=10000)

    print '\nLearning SVM...'
    if(performPCA):
        svm.learn(z, y)
    else:
        svm.learn(x,y)

    print '\nSaving the model at \'' + svmModelOut + '\'\n\n'
    svm.save_model(svmModelOut) #We will then load it to avoid the time consuming retraining
    print svm.label_nsv()


    options = {}
    options['barsBack'] = barsBack
    options['pca'] = myPCA
    options['dims'] = firstDimensions
    options['stockSymbol'] = stockSymbol
    options['performPCA'] = performPCA
    pickle.dump(options, open("options.p", "wb") )

    #ret, sharpe = predictionsfromsvm_model.main()

    #allRet.append(ret)
    #allSharpe.append(sharpe)

    return 0.0, 0.0
    #return ret, sharpe

if __name__=='__main__':
    runSim()
