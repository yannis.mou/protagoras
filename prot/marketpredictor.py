'''
The MIT License (MIT)

Copyright (c) 2013 Georgios Pierris - www.pierris.gr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''

import mlpy
import copy
import sys

import csv

class MarketPredictor:

    def __init__(self, modelFilename = None, modelType = 'svm'):

        if( modelType == 'svm' ):
            self.svm = mlpy.libsvm.LibSvm.load_model(modelFilename)

    def setMarketData(self, marketData):
        self.marketData = marketData


    def instantPredictionSVM(self, x):

        ret = self.svm.pred(x)
        return ret

    def predictFromSVM2(self, x, symbol=['AAPL'], daysBack=4):

        predictionMatrix = copy.deepcopy(self.marketData.stockdata)

        ret = self.svm.pred(x)

        for tIdx in range(daysBack, len(self.marketData.timestamps)):
            print tIdx, self.marketData.timestamps[tIdx], tIdx-daysBack, ret[tIdx-daysBack]
            predictionMatrix[symbol[0]][tIdx] = ret[tIdx-daysBack]

        #print predictionMatrix
        return predictionMatrix

    def predictFromSVM(self, x, model='', daysBack=2):

        if(model!=''): #Then we want to use a specific model
            svm = mlpy.libsvm.LibSvm.load_model(model)
        else:
            svm = self.svm

        predictionMatrix = copy.deepcopy(self.marketData.stockdata)

        for symbol in self.marketData.symbols:
            for i in range(5,len(predictionMatrix[symbol])):

                predictionMatrix[symbol][i] = self.predict(svm, symbol, i, daysBack)

        return predictionMatrix

    def predict(self, svm, symbol, tsIdx, daysBack = 4):
        sample = []
        try:
            timestamps = self.marketData.timestamps[tsIdx-daysBack:tsIdx]
            sample = (self.marketData.daily_returns[symbol][timestamps]).values
        except IndexError:
            return 0
        except:
            print sys.exc_info()[0]
            sys.exit()

        if(len(sample)==0):
            print 'Error!'
            return 0

        ret = svm.pred(sample)
        print sample, '\t', ret, self.marketData.daily_returns[symbol][timestamps[-1]]
        if(ret > 0.0):#self.marketData.daily_returns[symbol][timestamps[-1]]):
            return 1
        else:
            return -1
        return 0



    def savePredictionsForMT5(self, prediction, predictionCounter, basePath = ''):

        with open( basePath + 'prediction_' + str(predictionCounter) + '.txt', 'wb') as f:
            fileWriter = csv.writer(f, delimiter=',')
            fileWriter.writerow(prediction)


    def save_predictions_as_orders(self, predictionMatrix, outFilename='orders.csv', orderVolume = '1000'):

        #import random

        with open(outFilename, "wb") as f:
            fileWriter = csv.writer(f, delimiter=',')

            for symbol in self.marketData.symbols:

                for i in range(1, len(predictionMatrix[symbol])):
                    #print symbol, predictionMatrix[symbol][i]
                    if( predictionMatrix[symbol][i] == 1.0):

                        fileWriter.writerow([str(self.marketData.timestamps[i].year), str(self.marketData.timestamps[i].month), str(self.marketData.timestamps[i].day), symbol, 'Buy', orderVolume, ''])
                        try:
                            laterTimestamp = self.marketData.timestamps[i+2]
                        except:
                            laterTimestamp = self.marketData.timestamps[-1]

                        fileWriter.writerow([str(laterTimestamp.year), str(laterTimestamp.month), str(laterTimestamp.day), symbol, 'Sell', orderVolume, ''])

                    elif( predictionMatrix[symbol][i] == -1.0 ):

                        fileWriter.writerow([str(self.marketData.timestamps[i].year), str(self.marketData.timestamps[i].month), str(self.marketData.timestamps[i].day), symbol, 'Sell', orderVolume, ''])
                        try:
                            laterTimestamp = self.marketData.timestamps[i+2]
                        except:
                            laterTimestamp = self.marketData.timestamps[-1]

                        fileWriter.writerow([str(laterTimestamp.year), str(laterTimestamp.month), str(laterTimestamp.day), symbol, 'Buy', orderVolume, ''])



