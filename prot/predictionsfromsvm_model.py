'''
The MIT License (MIT)

Copyright (c) 2013 Georgios Pierris - www.pierris.gr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''


#import sys
import datetime as dt

import pickle

from prot import marketsimulator as MS
from prot import marketdata_accessor as MDA
from prot import marketpredictor as md
from prot import datapreparation as dp

def main():
    starting_cash = 50000
    orders_filename = 'orders.csv'
    outvalues_filename = 'values.csv'
    svm_model_filename = 'svm_model.dat'

    options = pickle.load( open( "options.p", "rb" ) )
    days_back = options['days_back']
    my_pca = options['pca']
    first_dimensions = options['dims']
    symbol_stock = options['stockSymbol']

#------------------------------------------------------------------------------
# Do exactly the same thing for Prediction.... FIXME: Refactor code here!
    market_data = MDA.MarketDataAccessor()

    start_day = dt.datetime(2012, 1, 1, 16)
    end_day = dt.datetime(2012, 9, 5, 16)
#symbols = market_data.set_symbols([], 'sp5002012')
# Just to quickly set and get the symbols
    symbols = market_data.set_symbols([symbol_stock, '$SPX', 'SPY'])

#Fetch market data all in once!
    print 'Please be patient... Fetching data...'
    print 'From', start_day, ' until ', end_day, '\nfor symbols:\n', symbols
    market_data.load_data_from_qstk(start_day,
                                end_day,
                                symbols,
                                data_items = ['actual_close'])
    print '\nFetching data completed. Execution continues...\n\n'


#We need only a subsection for learning, not all the sp500
    #symbolsForLearning = market_data.set_symbols([symbol_stock])

#Data preparation begins...
    print '\nPreparing data for SVM...'
    dataPrep = dp.DataPreparation(market_data)

    x, _ = dataPrep.prepareData_lastPrices(days_back=days_back)
#print x[:3]
#print np.size(x[:,0]), np.size(x[0,:])
#raw_input('press enter to continue...')

    feature = dataPrep.getfeature_stockorindex('$SPX', days_back=days_back)
    x = dataPrep.appendColumns(x, feature)

    feature = dataPrep.getfeature_stockorindex('SPY', days_back=days_back)
    x = dataPrep.appendColumns(x, feature)

#print x[:3]
#print np.size(x[:,0]), np.size(x[0,:])

    x = dataPrep.derivativeOf(x)
    x = dataPrep.appendColumns(x, dataPrep.derivativeOf(x))
#print x[:3]
#print np.size(x[:,0]), np.size(x[0,:])

#print '------------------------------'
#x = dataPrep.increaseDimensionality(x)
#x = dataPrep.increaseDimensionality(x)
#print x
#print np.size(x[:,0]), np.size(x[0,:])
#print '---------------------------------'

###########################
    x = my_pca.transform(x, first_dimensions)

###########################

#------------------------------------------------------------------------------
#DIFFERENT CODE FOR PREDICTION

#Then load svm and run prediction to create the orders.csv file

    marketPredictor = md.MarketPredictor(svm_model_filename, modelType='svm')
    marketPredictor.setMarketData(market_data)
    predictionMatrix = marketPredictor.predictFromSVM2(x,
                                                       symbol=[symbol_stock],
                                                       days_back=days_back)
#predictionMatrix = marketPredictor.predictFromSVM(x, days_back=4)

    marketPredictor.save_predictions_as_orders(predictionMatrix,
                                            orders_filename,
                                            orderVolume = '100')


#Then test the model in the real market!

    myMarketSimulator = MS.MarketSimulator(starting_cash,
                                           orders_filename,
                                           outvalues_filename)

    myMarketSimulator.loadOrders()

    myMarketSimulator.printOrders()
    myMarketSimulator.runSim()

    myMarketSimulator.printOrders()

    #myMarketSimulator.printStatistics()
    return myMarketSimulator.getStatistics()

if __name__=='__main__':
    main()
