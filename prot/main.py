#!/usr/bin/python
'''
The MIT License (MIT)

Copyright (c) 2013 Georgios Pierris - www.pierris.gr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''

import sys
import getopt

from prot import marketsimulator as MS
from prot import eventprofiler

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main(argv=None):
    if argv is None:
        argv = sys.argv

    try:
        try:
            opts, _ = getopt.getopt(argv[1:], "hec:o:v:", ["help", "eventprofiler", "cash=", "orders=", "values="])
        except getopt.error, msg:
            raise Usage(msg)

        # process options
        startingCash, ordersFilename, outValuesFilename, runEventProfiler = 0.0, "", "", False
        for o, a in opts:
            if o in ("-h", "--help"):
                myMarketSim = MS.MarketSimulator()
                myMarketSim.printUsage()
                return 2
            elif o in ("-c", "--cash"):
                startingCash = float(a)
            elif o in ("-o", "--orders"):
                ordersFilename = a
            elif o in ("-v", "--values"):
                outValuesFilename = a
            elif  o in ("-e", "--eventprofiler"):
                runEventProfiler = True

        #Being here seems everything went well (no error checking yet!) and we will start our simulator
        myMarketSimulator = MS.MarketSimulator(startingCash, ordersFilename, outValuesFilename)

        if( runEventProfiler ):
            myMarketSimulator.bindEventProfiler( eventprofiler.EventProfiler(ordersFilename) )
            eventsMat = myMarketSimulator.myEventProfiler.runEventProfiler()
            myMarketSimulator.myEventProfiler.saveEventToOrders(eventsMat, ordersFilename)

        myMarketSimulator.loadOrders()

        myMarketSimulator.printOrders()
        myMarketSimulator.runSim()

        myMarketSimulator.printOrders()

        myMarketSimulator.printStatistics()

    except Usage, err:
        print >>sys.stderr, err.msg
        print >>sys.stderr, "for help use --help"
        return 3 #That would be an exit for failure



if __name__ == '__main__':

    sys.exit(main())



